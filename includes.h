#pragma once

namespace CG_RT {

	enum ObjectType {
		lightSource = 0,
		diffuse = 1,
		reflection = 2,
		refraction = 3
	};
}
