#pragma once

#include "Ray.h"
#include "includes.h"

class Material {
  public:
  Material();
    Material( glm::vec3 _ambient, glm::vec3 _diffuse, glm::vec3 _specular, float _glossiness = 0.0f, float _reflection =0.0f ) :
			ambient(_ambient), diffuse(_diffuse), specular(_specular), glossiness(_glossiness), reflection(_reflection) {}
    glm::vec3 ambient; //Reflectivity in r/g/b channels
    glm::vec3 diffuse;   
    glm::vec3 specular;	
    float glossiness; //Specular intensity
    float reflection;
};

// The father class of all the objects displayed. Some features would be shared between objects, others will be overloaded.
class Object {
  public:
    Object(const glm::mat4 &transform = glm::mat4(1.0f), const Material &material = Material(), CG_RT::ObjectType objectType = CG_RT::diffuse);
    //  The keyword const here will check the type of the parameters and make sure no changes are made
    //  to them in the function. It's not necessary but better for robustness

    virtual bool Intersect(const Ray &ray, IntersectInfo &info) const { return false; }
    glm::vec3 Position() const { return glm::vec3(transform[3][0], transform[3][1], transform[3][2]); }
	void Position(glm::vec3 _pos) { transform[3][0] = _pos.x; transform[3][1] = _pos.y; transform[3][2] = _pos.z; }
    	const Material *MaterialPtr() const { return &material; }
	CG_RT::ObjectType GetObjectType() { return objectType; }

  protected:  //  The difference between protected and private is that the protected members will still be available in subclasses.
	glm::mat4 transform;  // Usually a transformation matrix is used to decribe the position from the origin.
    	Material material;
	Bounds bounds;

	CG_RT::ObjectType objectType;

	bool RayTriangleIntersect(const Ray &ray, IntersectInfo &info, glm::vec3 _v0, glm::vec3 _v1, glm::vec3 _v2) const ;
    	bool SolveQuadratic( const float &a, const float &b, const float &c, float &x0, float &x1 ) const;
};

//  For all those objects added into the scene. Describing them in proper ways and the implement of function Intersect() is what needs to be done.
//  Actually, it's also possible to use some other objects, but those geometries are easy to describe and the intersects are easier to calculate.
//  Try something else if you like, for instance, a box?

/* TODO: Implement */
class Sphere : public Object {

	public:
		Sphere() {}
		Sphere(float _radius, const glm::mat4 &transform = glm::mat4(1.0f), const Material &material = Material(), CG_RT::ObjectType _type = CG_RT::diffuse );
								
    		virtual bool Intersect(const Ray &ray, IntersectInfo &info) const;  //  To figure out if the Ray hit this object.

		float Radius() { return radius; }

	protected:
		float radiusSqrt; //good friend to have around
		float radius;
};

/* TODO: Implement */
class Plane : public Object {
	public:
		Plane(glm::vec3 _v0, glm::vec3 _v1, glm::vec3 _v2, glm::vec3 _v3, const glm::mat4 &transform = glm::mat4(1.0f), const Material &material = Material(), CG_RT::ObjectType _type = CG_RT::diffuse);
		virtual bool Intersect(const Ray &ray, IntersectInfo &info) const;

	private:
		glm::vec3 v0, v1, v2, v3;
};

/* TODO: Implement */
class Triangle : public Object {
	public:
		Triangle(glm::vec3 _v0, glm::vec3 _v1, glm::vec3 _v2, const glm::mat4 &transform = glm::mat4(1.0f), const Material &material = Material(), CG_RT::ObjectType _type = CG_RT::diffuse);
  
    		virtual bool Intersect(const Ray &ray, IntersectInfo &info) const;

	private:
		glm::vec3 v0, v1, v2;
};
