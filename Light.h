#pragma once

#include "includes.h"
#include "Ray.h"
#include "Object.h"

class Material;

class Light : public Sphere {

public :
	Light( glm::mat4 _position, float _radius, glm::vec3 _col,float _intensity, CG_RT::ObjectType _objectType ) : intensity(_intensity), colour(_col), Sphere(_radius, _position) {objectType = _objectType;}
	Light() {}
	~Light() {};

	virtual bool Intersect(const Ray &ray, IntersectInfo &info) const;
	glm::vec3 Colour() { return colour; }
	float Intensity() { return intensity; }

protected:  //  The difference between protected and private is that the protected members will still be available in subclasses.
	glm::vec3 colour;
	float intensity;

private :


};
