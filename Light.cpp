#include "Light.h"
#include <iostream>

bool Light::Intersect(const Ray &ray, IntersectInfo &info) const {

	if( Sphere::Intersect(ray, info) ) {
		
		info.lastObjectHitType = CG_RT::lightSource;
		return true;
	}
	return false;
}
