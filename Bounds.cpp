#include "Bounds.h"
#include <algorithm>
#include <iostream>
#include "RayTracer.h"

//ray cube intersections achieved using slabs check, good for lack of branching 
//- this would implement well with octrees, quadtrees and run well on GPU as glsl or an opencl kernel
// (don't branch on the GPU!)
bool Bounds::IsColliding( const Ray &ray ) const {
	//return true;
	double t1 = (min[0] - ray.origin[0]) * ray.inv_direction[0];
	double t2 = (max[0] - ray.origin[0]) * ray.inv_direction[0];
	
	double tmin = std::min( t1, t2 );
	double tmax = std::max( t1, t2 );
	
	for( int i=0; i<3; i++ ) {
		t1 = (min[i] - ray.origin[i]) * ray.inv_direction[i];
		t2 = (max[i] - ray.origin[i]) * ray.inv_direction[i];
		
		tmin = std::max( tmin, std::min( std::min(t1,t2), tmax ));
		tmax = std::min( tmax, std::max( std::max(t1,t2), tmin ));
	}
	
	return tmax > std::max(tmin, 0.0);
}

void Bounds::SetSphere( glm::vec3 _centre, float _rad ) {

	dimensions = glm::vec3( _rad * 2.f );

	glm::vec3 halfDim = dimensions * 0.5f;
	
	min = _centre - halfDim;
	max = _centre + halfDim;

	boundsCentre = _centre;
}

void Bounds::SetPoints( std::vector<glm::vec3> _points ) {

	max = glm::vec3(-1.f * std::numeric_limits<float>::max());
	min = glm::vec3(std::numeric_limits<float>::max());

	for(int i = 0; i< _points.size(); i++){
		for(int j = 0; j < 3; j++){
			if(min[j] > _points[i][j]){
				min[j] = _points[i][j];
			}
			if(max[j] < _points[i][j]){
				max[j] = _points[i][j];
			}
		}
	}
	//ensure no flat AABBs
	float epsilon = std::numeric_limits<float>::epsilon();
	for(int i = 0; i < 3; i++){

		if(glm::abs(min[i] - max[i]) < epsilon) {
			min[i] -= epsilon * 100.f;
			max[i] += epsilon * 100.f;
		}
	}
	
	dimensions.x = glm::abs(min.x - max.x);
	dimensions.z = glm::abs(min.z - max.z);
	dimensions.y = glm::abs(min.y - max.y);

	boundsCentre = min + ( (max - min) / 2.0f);
	

	//std::cout << "Bounds Min: " << glm::to_string(min) << " Bounds max : "<< glm::to_string(max) << std::endl;
}
