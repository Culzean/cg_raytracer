CC=g++
CFLAGS= -03 
LIBS= -lGL -lglut

all:
	g++ *.cpp $(LIBS) -o RayTracer

run: all
	./RayTracer

clean: 
	rm -f RayTracer *.o
