#include "Object.h"
#include <algorithm>
#include <utility>
#include <iostream>
#include <vector>
#include "glm/gtx/norm.hpp"
#include "glm/ext.hpp"

using namespace std;

Material::Material():
    ambient(1.0f),
    diffuse(1.0f),
    specular(1.0f),
    glossiness(10.0f)
  {}

Object::Object(const glm::mat4 &transform, const Material &material, CG_RT::ObjectType _type):
    transform(transform),
    material(material),
    objectType(_type)
  {}

Plane::Plane(glm::vec3 _v0, glm::vec3 _v1, glm::vec3 _v2, glm::vec3 _v3, const glm::mat4 &transform, const Material &material, CG_RT::ObjectType _type) : Object(transform, material, _type) {
	glm::vec4 transLated0 = transform * glm::vec4(_v0, 1.0f);
	v0 = glm::vec3(transLated0);
	glm::vec4 transLated1 = transform * glm::vec4(_v1, 1.0f);
	v1 = glm::vec3(transLated1);
	glm::vec4 transLated2 = transform * glm::vec4(_v2, 1.0f);
	v2 = glm::vec3(transLated2);
	glm::vec4 transLated3 = transform * glm::vec4(_v3, 1.0f);
	v3 = glm::vec3(transLated3);
	std::vector<glm::vec3> points = vector<glm::vec3>();
	points.push_back(v0);
	points.push_back(v1);
	points.push_back(v2);
	points.push_back(v3);
	bounds.SetPoints(points);
}

Triangle::Triangle(glm::vec3 _v0, glm::vec3 _v1, glm::vec3 _v2, const glm::mat4 &transform, const Material &material, CG_RT::ObjectType _type) : Object(transform, material, _type) {

	glm::vec4 transLated0 = transform * glm::vec4(_v0, 1.0f);
	v0 = glm::vec3(transLated0);
	glm::vec4 transLated1 = transform * glm::vec4(_v1, 1.0f);
	v1 = glm::vec3(transLated1);
	glm::vec4 transLated2 = transform * glm::vec4(_v2, 1.0f);
	v2 = glm::vec3(transLated2);
	std::vector<glm::vec3> points = vector<glm::vec3>();
	points.push_back(v0);
	points.push_back(v1);
	points.push_back(v2);
	bounds.SetPoints(points);
}

Sphere::Sphere(float _radius, const glm::mat4 &transform, const Material &material, CG_RT::ObjectType _type ) : radius(_radius), radiusSqrt(_radius * _radius), Object(transform, material, _type) {
	glm::vec3 centre = glm::vec3 (transform[3][0], transform[3][1], transform[3][2]);
	bounds.SetSphere(centre, _radius);			
}

bool Object::RayTriangleIntersect(const Ray &ray, IntersectInfo &info, glm::vec3 _v0, glm::vec3 _v1, glm::vec3 _v2) const {

	if(!bounds.IsColliding(ray)) {
		return false;
	}

	//cout << "Check triangle " << endl;

	glm::vec3 baryPosition = glm::vec3(0.0f);
	glm::vec3 edge0 = _v1 - _v0;
	glm::vec3 edge1 = _v2 - _v0;
	//don't this to be normalised
	glm::vec3 normal = glm::cross(edge0, edge1);

	glm::vec3 p = glm::cross(ray.direction, edge1);

	float a = glm::dot(edge0, p);

	//first, check this is not psychotic
	float rayTriDot = glm::dot(normal, ray.direction);
	float epsilon = std::numeric_limits<float>::epsilon();

	if (glm::abs(rayTriDot) <= epsilon) {
		return false; //this ray is parallel to triangle
	}
	float d = glm::dot(normal, _v0);

	float t = (glm::dot(normal, ray.origin) + d);

	if (a< 0.0f) {
		//this collision is behind the camera
		return false;
	}

	float f = (1.0f) / a;

	float time = ( glm::dot((normal), ( _v0 - ray.origin) ) ) / (rayTriDot);
	glm::vec3 hit = ray(t);


	glm::vec3 s = ray.origin - _v0;
	baryPosition.x = f * glm::dot(s, p);
	if (baryPosition.x < 0.0f)
		return false;
	if (baryPosition.x > 1.0f)
		return false;

	glm::vec3 q = glm::cross(s, edge0);
	baryPosition.y = f * glm::dot(ray.direction, q);
	if (baryPosition.y < 0.0f)
		return false;
	if (baryPosition.y + baryPosition.x > 1.0f)
		return false;

	baryPosition.z = f * glm::dot(edge1, q);

	if (baryPosition.z >= 0.0f && info.time > time) {
		//cout << "Plane collision point : " << glm::to_string(ray(time)) << " and time : " << time << endl;
		info.time = time;
		info.material = &this->material;
		info.hitPoint = ray(time);
		info.normal = glm::normalize(normal);
		info.lastObjectHitType = CG_RT::diffuse;
		return true;
	}
}

bool Object::SolveQuadratic( const float &a, const float &b, const float &c, float &x0, float &x1 ) const  {

	float discriminant = b * b - (4.0f * a * c);
	if(discriminant < 0) {
		//no solution
		return false;
	}
	else if (discriminant == 0) {
	//a single solution
		x0 = x1 = -0.5f * b * a;
	}
	else {
		//there are two solutions
		float q = (b > 0) ?
		-0.5f * ( b + glm::sqrt( discriminant ) ) :
		-0.5f * ( b - glm::sqrt( discriminant ) );
		x0 = q / a;
		x1 = c / q;
	}
	if ( x0 > x1 ) {
		std::swap( x0, x1 );
	}
	return true;
}


bool Sphere::Intersect(const Ray &ray, IntersectInfo &info) const { 

	if( !bounds.IsColliding( ray ) ) {
		return false;
	}

	float t0, t1 = 0.0f;
	glm::vec3 toSphere = ray.origin - this->Position();
	//
	float a = glm::dot(ray.direction, ray.direction );
	float b = 2 * glm::dot( ray.direction, toSphere);
	float c =  glm::dot(toSphere, toSphere) - radiusSqrt;
	//cout << " sphere rad ^2 : " << radius << endl;
	if( !SolveQuadratic(a,b,c,t0,t1) ) return false;
	
	if( t0 > t1 ) {
		std::swap(t0,t1);
	}
	
	if( t0 < 0.f ) {
		//this is behind us
		t0 = t1;
		if(t0 < 0.0f) return false; //this sphere is behind the camera 
	}
	
	//info.hitPoint = 
	if ( info.time > t0 ) {
		//cout << "Sphere hit at time : " << t0 << endl;
		info.time = t0;
		info.hitPoint = ray(t0);
		glm::vec3 surfaceNormal = info.hitPoint - Position();
		info.normal = glm::normalize(surfaceNormal);
		info.material = &this->material;
		info.lastObjectHitType = CG_RT::diffuse;
	}
}
// Function glm::dot(x,y) will return the dot product of parameters. (It's the inner product of vectors)

/* TODO: Implement */
bool Plane::Intersect(const Ray &ray, IntersectInfo &info) const { 
	bool ret = false;

	//if(!bounds.IsColliding(ray)) {
	//	return ret;
	//}


	ret = this->RayTriangleIntersect( ray, info, v0, v3, v1 );
	if ( !ret ) {
		ret = this->RayTriangleIntersect(ray, info, v0, v1, v2);
	}
	
	//cout << "Hello square " << ret << endl;
	return ret;
}

/* TODO: Implement */
bool Triangle::Intersect(const Ray &ray, IntersectInfo &info) const {

	return this->RayTriangleIntersect( ray, info, v0, v1, v2 );
}
