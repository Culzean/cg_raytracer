#pragma once

#include "Ray.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/string_cast.hpp"
#include <vector>

class Ray;

class Bounds {

	private:
		glm::vec3 boundsCentre;
		glm::vec3 min, max;
		glm::vec3 dimensions;
		
	public:
		Bounds(){};
		bool IsColliding( const Ray &ray ) const;
		
		//set points after they have been translated into scene space
		//there is more robust ways to do this if we expected the geometry to move, but we don't
		void SetSphere( glm::vec3 _centre, float _rad );
		void SetPoints( std::vector<glm::vec3> _points );	
		
};
