#include "RayTracer.h"
#include <iostream>
#include "glm/ext.hpp"
#include "Light.h"
#include <ctime>

int windowX = 640;
int windowY = 480;

using namespace std;
using namespace glm;

/*
** std::vector is a data format similar with list in most of  script language, which allows users to change its size after claiming.
** The difference is that std::vector is based on array rather than list, so it is not so effective when you try to insert a new element, but faster while calling for values randomly or add elements by order.
*/
std::vector<Object*> objects;
vector<Light*> lights;
glm::vec3 backgroundColor = glm::vec3( 0, 102/255, 153/255 );
vec3 eyePosition = vec3(-10, 10, 10);
static const float screenGamma = 2.2f;
vec3 globalAmbient( 0.088f, 0.088f, 0.088f );
const float softShadowCasts = 12.f;
const int TraceDepth = 3;
clock_t rendTime = 0.f;
const bool superSample = false;

void cleanup() {
	for(unsigned int i = 0; i < objects.size(); ++i){
		if(objects[i]){
			delete objects[i];
		}
	}
}

/*
** TODO: Function for testing intersection against all the objects in the scene
**
** If an object is hit then the IntersectionInfo object should contain
** the information about the intersection. Returns true if any object is hit, 
** false otherwise
** 
*/
bool CheckIntersection(const Ray &ray, IntersectInfo &info) {
	bool ret = false;
	for( int i=0; i<objects.size(); i++ ) {
		Object* object = objects[i];
		
		if (object->Intersect(ray, info)) {
			//cout << "Condition " << i << endl;
			ret = true;
		}
	}
	
	return ret;
	//You need to add your own solution for this funtion, to replace the 'return true'.	
	//Runing the function Intersect() of each of the objects would be one of the options.
	//Each time when intersect happens, the payload of ray will need to be updated, but that's no business of this function.
	//To make it clear, keyword const prevents the function from changing parameter ray.
}

glm::vec3 BlinnPhongShading(Light* light, Payload const payload, IntersectInfo const info) {

	vec3 surfaceNormal;
	vec3 vecToLight;
	vec3 lightOnSurface = glm::vec3(0.0f);
	static const float attenuationCutOff = 0.01f;

	//find distance to light and calculate the attentuation
	float distToLight = glm::distance( info.hitPoint, light->Position() );
	float attenDenom = (distToLight / (light->Radius() * 3.8f)) + 1.f;
	float attenuationFactor = 1.0f / (attenDenom * attenDenom);
	//attenuationFactor = 1.0f / distToLight;
	//apply a cut off to the value
	attenuationFactor = (attenuationFactor - attenuationCutOff) / ( 1 - attenuationCutOff );
	attenuationFactor = glm::max(attenuationFactor, 0.0f);
	//cout << "Attenuation Factor as distance " << distToLight << ",  " << attenuationFactor;

	vec3 lightDir = normalize(light->Position() - info.hitPoint);

	float lambertian = glm::max(dot(lightDir, info.normal), 0.0f);
	float specular = 0.0f;

	if (lambertian > 0.0f) {

		vec3 viewDir = glm::normalize(eyePosition - info.hitPoint);

		vec3 halfVec = glm::normalize(lightDir + viewDir);
		float specAngle = glm::max(dot(halfVec, info.normal), 0.0f);
		specular = glm::pow(specAngle, info.material->glossiness);

	}
	vec3 colorLinear = attenuationFactor * info.material->ambient * light->Colour() * light->Intensity() +
		(lambertian * attenuationFactor * info.material->diffuse * light->Colour() * light->Intensity())+
		specular * attenuationFactor * info.material->specular * light->Colour() * light->Intensity();

	//cout << "Colour in Blinn " << glm::to_string(colorLinear) << endl;
	return colorLinear;
	lightOnSurface = glm::pow(colorLinear, vec3(1.0f / screenGamma));
	return lightOnSurface;

}

float CheckShadowRay( glm::vec3 _surfacePoint, Light* _light ) {
	float ret = 0.f;
	Payload payload;
	IntersectInfo info;
	
	//cast ray from surface intersection to each light source
	//we wish to cast the ray starting slightly away from the current surface, or else we will get false results
	glm::vec3 rayDir = glm::normalize(glm::vec3(_light->Position() - _surfacePoint));
	glm::vec3 castPoint = _surfacePoint + ( rayDir * 0.02f ); 
	Ray shadowRay = Ray(castPoint,rayDir ); //Ray(const glm::vec3 &origin, const glm::vec3 &direction)
	float narrowRad = _light->Radius() / 5.f;

	for( int j=0; j<softShadowCasts; j++ ) {
		info.lastObjectHitType = CG_RT::lightSource;
		info.time = -1.f;
		glm::vec3 randSpherePos = glm::sphericalRand( narrowRad );
		rayDir = glm::normalize(glm::vec3( (_light->Position() + randSpherePos) - _surfacePoint));
		castPoint = _surfacePoint + ( rayDir * 0.02f ); 
		shadowRay = Ray(castPoint,rayDir ); //Ray(const glm::vec3 &origin, const glm::vec3 &direction)
		
		for( int i=0; i<objects.size(); i++ ) {
			Object* object = objects[i];
			
			if(object->Intersect(shadowRay, info)) {
				//cout << "shadow ray has hit someting " << endl;
				info.lastObjectHitType = CG_RT::diffuse;
				info.time = 1.f;
				break;
			}
		}
	
		if( info.lastObjectHitType == CG_RT::lightSource || info.time < 0.0f ) {
			ret += 1.0f / softShadowCasts;
		}
	}	

	return ret;
}


/*
** TODO: Recursive ray-casting function. It might be the most important Function in this demo cause it's the one decides the color of pixels.
**
** This function is called for each pixel, and each time a ray is reflected/used 
** for shadow testing. The Payload object can be used to record information about
** the ray trace such as the current color and the number of bounces performed.
** This function should return either the time of intersection with an object
** or minus one to indicate no intersection.
*/
//	The function CastRay() will have to deal with light(), shadow() and reflection(). The impement of them would also be important.
float CastRay(Ray &ray, Payload &payload) {

	//cout << "Cast ray has bounced " << payload.numBounces << endl;
	IntersectInfo info;

	//cout << "Ray to test " << glm::to_string(ray.direction) << " at location " << glm::to_string(ray.origin) << endl;
	if ( CheckIntersection(ray,info)) {
	
		//we have a bouce event
		if( payload.numBounces == 0 ) {
			//payload.color += info.material->ambient * globalAmbient;
		}
		payload.numBounces += 1;

		glm::vec3 diffuseCol = payload.color;
		for (int i = 0; i < lights.size(); i++ ) {
			Light* light = lights[i];

			float shadowWeight = CheckShadowRay( info.hitPoint, light );
			if( shadowWeight > 0.f) {
				diffuseCol += shadowWeight * BlinnPhongShading( light, payload, info );
			}
		}

		//fire reflection
		glm::vec3 reflection = ray.direction - 2*(glm::dot(ray.direction,info.normal)) * info.normal;
		glm::vec3 reflectPoint = info.hitPoint + (reflection * 0.000002f);
		Ray reflectedRay = Ray( reflectPoint, reflection );
		
		
		if(payload.numBounces < TraceDepth) {
			CastRay(reflectedRay, payload);
		}
		glm::vec3 reflectedCol = payload.color * info.material->reflection;

		//cout << "Intersection found " << info.material << endl;


		//cout << "Assign color to payload " << endl;
		
		//find the lighting for each light in scene
		
		payload.color = reflectedCol + (1-info.material->reflection) * diffuseCol;
		
		
		
		// In this case, it's just because we want to show something and we do not want to show the same color for every pixel.
		// Usually payload.color will be decided by the bounces.
		return info.time;
	}
	else{
		//payload.color = glm::vec3(0.f);
		//cout << "And exit " << endl;
		//payload.color = glm::vec3(1.0f);
		// The Ray from camera hits nothing so nothing will be seen. In this case, the pixel should be totally black.
		return -1.0f;	
	}	
}

// Render Function

// This is the main render function, it draws pixels onto the display
// using GL_POINTS. It is called every time an update is required.

// This function transforms each pixel into the space of the virtual
// scene and casts a ray from the camera in that direction using CastRay
// And for rendering,
// 1)Clear the screen so we can draw a new frame
// 2)Cast a ray into the scene for each pixel on the screen and use the returned color to render the pixel
// 3)Flush the pipeline so that the instructions we gave are performed.

void Render()
{
	rendTime = clock();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// Clear OpenGL Window

	//	Three parameters of lookat(vec3 eye, vec3 center, vec3 up).	
	glm::mat4 viewMatrix = glm::lookAt(eyePosition, glm::vec3(0.0f,0.0f,0.0f), glm::vec3(0.0f,1.0f,0.0f));
	glm::mat4 projMatrix = glm::perspective(45.0f, (float)windowX / (float)windowY, 1.0f, 10000.0f);

	glBegin(GL_POINTS);	//Using GL_POINTS mode. In this mode, every vertex specified is a point.
	//	Reference https://en.wikibooks.org/wiki/OpenGL_Programming/GLStart/Tut3 if interested.
  

	float sampleRate = superSample ? 0.0625f : 1.0f;
	for(int x = 0; x < windowX; ++x)
		for(int y = 0; y < windowY; ++y){//Cover the entire display zone pixel by pixel, but without showing.
			glm::vec3 sample = glm::vec3(0.0f);
			float wholePixelX = 2 * ((x + 0.5f) / windowX) - 1;
			float wholePixelY = -2 * ((y + 0.5f) / windowY) + 1;
			for (float subX =  superSample ? 0.0f : 0.5f; subX < 1.0f; subX+= (superSample ? 0.25f : 1.0f)) {
				for (float subY =  superSample ? 0.0f : 0.5f; subY < 1.0f; subY+= (superSample ? 0.25f : 1.0f)) {

					float pixelX = 2 * ((x + subX) / windowX) - 1;	//Actually, (pixelX, pixelY) are the relative position of the point(x, y).
					float pixelY = -2 * ((y + subY) / windowY) + 1;	//The displayzone will be decribed as a 2.0f x 2.0f platform and coordinate origin is the center of the display zone.

																	//	Decide the direction of each of the ray.
					glm::vec4 worldNear = glm::inverse(viewMatrix) * glm::inverse(projMatrix) * glm::vec4(pixelX, pixelY, -1, 1);
					glm::vec4 worldFar = glm::inverse(viewMatrix) * glm::inverse(projMatrix) * glm::vec4(pixelX, pixelY, 1, 1);
					glm::vec3 worldNearPos = glm::vec3(worldNear.x, worldNear.y, worldNear.z) / worldNear.w;
					glm::vec3 worldFarPos = glm::vec3(worldFar.x, worldFar.y, worldFar.z) / worldFar.w;

					Payload payload;
					Ray ray(worldNearPos, glm::normalize(glm::vec3(worldFarPos - worldNearPos))); //Ray(const glm::vec3 &origin, const glm::vec3 &direction)

					if (CastRay(ray,payload) > 0.0f) {

						sample += sampleRate * (payload.color + globalAmbient);
					}
					else {
						sample += sampleRate * backgroundColor;
					}

				}

			}

			glColor3f(sample.x, sample.y, sample.z);
			
			glVertex3f(wholePixelX, wholePixelY, 0.0f);
			//cout << "Ray direction : " << glm::to_string( glm::normalize(glm::vec3(worldFarPos - worldNearPos) ) )<< endl;


		}
  
  	std::cout << "render time: " << float( clock () - rendTime ) /  CLOCKS_PER_SEC << endl;
  
	glEnd();

	glFlush();

	//Sleep(1);
}

glm::mat4 GetTransform( float p0,float p1,float p2 ) {
	return glm::translate(glm::mat4(1.0f), glm::vec3(p0,p1,p2) );
}

int main(int argc, char **argv) {

  	//initialise OpenGL
	glutInit(&argc, argv);
	//Define the window size with the size specifed at the top of this file
	glutInitWindowSize(windowX, windowY);

	//Create the window for drawing
	glutCreateWindow("RayTracer");
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);	

	cout << "Display drivers used " << glGetString(GL_VENDOR) << endl;

	//Set the function demoDisplay (defined above) as the function that
	//is called when the window must display.
	glutDisplayFunc(Render);
  
	//	TODO: Add Objects to scene
	//require colours for ambient, diffuse and specular
	Material mat0 = Material( glm::vec3(0.245f, 0.54f, 0.183f), glm::vec3(0.245f, 0.54f, 0.183f), glm::vec3(0.5f, 0.5f, 0.5f) );
	Material mat1 = Material( glm::vec3(0.245f, 0.183f, 0.54f), glm::vec3(0.245f, 0.183f, 0.54f), glm::vec3(0.5f, 0.5f, 0.5f) );
	Material mat2 = Material(glm::vec3(0.245f, 0.183f, 0.54f), glm::vec3(0.245f, 0.183f, 0.54f), glm::vec3(0.5f, 0.5f, 0.5f));
	Material gloss = Material(glm::vec3(0.245f, 0.183f, 0.54f), glm::vec3(0.245f, 0.183f, 0.54f), glm::vec3(0.5f, 0.5f, 0.5f), 21.23f);
	Material mirrorMat0 = Material( glm::vec3(0.125f, 0.1123f, 0.12f), glm::vec3(0.125f, 0.123f, 0.12f), glm::vec3(0.7f, 0.7f, 0.7f), 8.23f, 0.38f);
	Material mirrorMat1 = Material( glm::vec3(0.145f, 0.1123f, 0.12f), glm::vec3(0.145f, 0.123f, 0.12f), glm::vec3(0.1f, 0.1f, 0.1f), 8.23f, 1.0f);
	glm::mat4 starPos =  glm::mat4(1.0f);
	glm::mat4 triPos0 = glm::mat4(1.0f);
	glm::mat4 planePos = glm::translate( glm::mat4(1.0f), vec3( -25.0f, -0.8f, -25.0f ) );

	//let there be light
	glm::mat4 lightPos = glm::mat4(1.0f);
	starPos = glm::translate(lightPos, glm::vec3(-8.f, 7.5f, -2.f));
	Light* light0 = new Light(starPos, 1.52f, glm::vec3( 0.82f, 0.86, 0.84 ) ,1.192f, CG_RT::lightSource );
	lights.push_back(light0);
	starPos = glm::translate(lightPos, glm::vec3(-6.f, 8.5f, -8.8f));
	Light* light1 = new Light(starPos, 2.72f, glm::vec3( 0.47f, 0.89, 0.74 ) ,1.192f, CG_RT::lightSource );
	lights.push_back(light1);

	//mirror surface
	glm::mat4 mirrorPos0 = GetTransform( 7.2f, -1.0f, -5.0f );
	Plane* mirror0 = new Plane( glm::vec3(0.0f, 0.0f, 18.0f), glm::vec3(0.0f, 18.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 18.0f, 18.0f), mirrorPos0, mirrorMat0 );
	objects.push_back(mirror0);

	Sphere* sphere = new Sphere(1.0f, starPos, mat0);
	Triangle* triangle0 = new Triangle( glm::vec3(-5.0f,0.0f,0.0f), glm::vec3(-0.5f,0.0f,0.0f), glm::vec3(-1.0f,2.0f,0.0f) , triPos0, gloss );
	Plane* plane0 = new Plane(glm::vec3(0.0f, 0.0f, 50.0f), glm::vec3(50.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(50.0f, 0.0f, 50.0f), planePos, mirrorMat1);
	
	glm::mat4 triPos1 = glm::translate(glm::mat4(1.0f) , glm::vec3(-5.f, 0.45f, -5.f));
	Triangle* triangle1 = new Triangle(glm::vec3(0.0f, 0.0f, 5.0f), glm::vec3(5.0f, 5.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), triPos1, mirrorMat0);

	Material axisMat = Material(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.245f, 0.54f, 0.183f), glm::vec3(0.5f, 0.5f, 0.5f));
	for (int i = 0; i < 7; i++ ) {
		glm::mat4 transform = glm::mat4(1.0f);
		transform[3][0] += 0.5f * i;
		Sphere* sphere0 = new Sphere(0.3f, transform, axisMat);
		objects.push_back( sphere0 );
	}
	axisMat = Material(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.245f, 0.54f, 0.183f), glm::vec3(0.5f, 0.5f, 0.5f));
	for (int i = 0; i < 7; i++) {
		glm::mat4 transform = glm::mat4(1.0f);
		transform[3][1] += 0.5f * i;
		Sphere* sphere0 = new Sphere(0.3f, transform, axisMat);
		objects.push_back(sphere0);
	}
	axisMat = Material(glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.245f, 0.54f, 0.183f), glm::vec3(0.5f, 0.5f, 0.5f));
	for (int i = 0; i < 7; i++) {
		glm::mat4 transform = glm::mat4(1.0f);
		transform[3][2] += 0.5f * i;
		Sphere* sphere0 = new Sphere(0.3f, transform, axisMat);
		objects.push_back(sphere0);
	}
	
	glm::mat4 spherePos = glm::translate(glm::mat4(1.0f), glm::vec3(-1.2f, 1.6f, -8.f));
	Sphere* sphere2 = new Sphere(2.8f, spherePos, mirrorMat0);
	
	objects.push_back( sphere );
	objects.push_back( sphere2 );
	//objects.push_back( triangle0 );
	objects.push_back(triangle1);
	objects.push_back( plane0 );
	
	//	This part is related to function CheckIntersection().
	//	Being added into scene means that the object will take part in the intersection checking, so try to make these two connected to each other.

	atexit(cleanup);
	glutMainLoop();
}
